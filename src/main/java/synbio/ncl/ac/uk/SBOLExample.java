package synbio.ncl.ac.uk;

import org.sbolstandard.core.DnaComponent;
import org.sbolstandard.core.SBOLDocument;
import org.sbolstandard.core.SBOLFactory;
import org.sbolstandard.core.SequenceAnnotation;
import org.sbolstandard.core.StrandType;

import synbio.ncl.ac.uk.util.SBOLHelper;

public class SBOLExample {

	public  String GetSBOLTest() throws Exception
	{		
		String baseURL="http://fakeuri.org";
		SBOLHelper sbolHelper=new SBOLHelper();
		SBOLDocument document=SBOLFactory.createDocument();
		
		DnaComponent prom1 = sbolHelper.GetDnaComponent(baseURL + "/part/prom1", "prom1", "Promoter 1", "An example promoter", "http://purl.org/obo/owl/SO#SO_0000167","aaa");
		DnaComponent dnaComponent2 = sbolHelper.GetDnaComponent(baseURL + "/part/doubleprom1", "doubleprom1", "Double Promoter 1", "A double promoter example", "http://purl.org/obo/owl/SO#SO_0000167","aaaaaa");
		SequenceAnnotation annotation1=sbolHelper.GetSequenceAnnotation(baseURL + "/part/doubleprom1_1_3", 1, 3, StrandType.POSITIVE, prom1);
		SequenceAnnotation annotation2=sbolHelper.GetSequenceAnnotation(baseURL + "/part/doubleprom1_4_6", 4, 6, StrandType.POSITIVE, prom1);
		dnaComponent2.addAnnotation(annotation1);
		dnaComponent2.addAnnotation(annotation2);
						
		document.addContent(dnaComponent2);
		String output=sbolHelper.Serialize(document);
		return output;
	}
}
