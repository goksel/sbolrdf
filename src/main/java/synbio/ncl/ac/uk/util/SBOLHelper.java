package synbio.ncl.ac.uk.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URI;

import org.sbolstandard.core.DnaComponent;
import org.sbolstandard.core.DnaSequence;
import org.sbolstandard.core.SBOLDocument;
import org.sbolstandard.core.SBOLFactory;
import org.sbolstandard.core.SequenceAnnotation;
import org.sbolstandard.core.StrandType;

public class SBOLHelper {

	/*public static String GetSBOLTest() throws Exception
	{		
		String baseURL="http://fakeuri.org";
		SBOLDocument document=SBOLFactory.createDocument();
		Collection collection=SBOLFactory.createCollection();
		collection.setURI(new URI(baseURL + "/genericparts"));
		collection.setDisplayId("GenericParts");
		
		DnaComponent dnaComponent = SBOLFactory.createDnaComponent();
		dnaComponent.setName("test part");
		dnaComponent.setDescription("test description");
		dnaComponent.setDisplayId("testid");
		dnaComponent.setURI(new URI(baseURL + "/part/test" ));
		dnaComponent.addType(new URI("http://purl.org/obo/owl/SO#SO_0000167"));
		
		DnaSequence sequence=SBOLFactory.createDnaSequence();		
		sequence.setNucleotides("acccggaaagggccc");		
		sequence.setURI(new URI(baseURL + "/part/test/NA"));			
		dnaComponent.setDnaSequence(sequence);	
		
		collection.addComponent(dnaComponent);
		document.addContent(collection);
		String output=Serialize(document);
		return output;
	}*/
	private static String baseURL="http://www.fakeuri.org";
	
	
	
	public SequenceAnnotation GetSequenceAnnotation(String uri,int start,int end, StrandType strandType,DnaComponent dnaComponent) throws Exception
	{
		SequenceAnnotation annotation=SBOLFactory.createSequenceAnnotation();
		annotation.setURI(new URI(uri));
		annotation.setBioStart(start);
		annotation.setBioEnd(end);
		annotation.setStrand(strandType);
		annotation.setSubComponent(dnaComponent);
		return annotation;		
	}
	public DnaComponent GetDnaComponent(String uri, String displayId, String name,String description, String type,String nucleotideSequence) throws Exception
	{
		DnaComponent dnaComponent = SBOLFactory.createDnaComponent();
		dnaComponent.setName(name);
		dnaComponent.setDescription(description);
		dnaComponent.setDisplayId(displayId);
		dnaComponent.setURI(new URI(uri));
		dnaComponent.addType(new URI(type));
		DnaSequence sequence=SBOLFactory.createDnaSequence();		
		sequence.setNucleotides(nucleotideSequence);		
		sequence.setURI(new URI(uri + "/NA"));			
		dnaComponent.setDnaSequence(sequence);
		return dnaComponent;
	}
		
	public static String Serialize(SBOLDocument document) throws Exception
	{
		ByteArrayOutputStream buffer=new ByteArrayOutputStream();  
		SBOLFactory.write(document, buffer);
		String xml=buffer.toString(); 
		return xml;		
	}
	
	public static SBOLDocument Read(String filePath) throws Exception
	{
		String sbolData=FileHelper.GetFileContent(new File(filePath));
		SBOLDocument sbolDocument=SBOLFactory.read(new ByteArrayInputStream(sbolData.getBytes()));
		return sbolDocument;
	}
	
}
