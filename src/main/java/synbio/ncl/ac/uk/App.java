package synbio.ncl.ac.uk;

import synbio.ncl.ac.uk.util.SBOLHelper;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        SBOLExample sbolExample=new SBOLExample();
        String sbolData=sbolExample.GetSBOLTest();
        System.out.println("-----------------------------------------------SBOL-----------------------------------------------");        
        System.out.println(sbolData);
        
        SBOLRDFWriterExample rdfExample=new SBOLRDFWriterExample();
        rdfExample.GenerateRDFExamples();
    }
}
